<?php 

function __autoload($className){
	//echo $className."<br/>";
	$backSpaces = './../../';
	$fileName = $backSpaces . str_replace("\\","/",$className) . ".php";
	//echo $fileName."<br/>";
	require_once($fileName);
}

session_start();

use App\Engine\Core\DB;
use App\Engine\Core\Auth;
use App\Engine\Tool\Msg;
use App\Engine\Tool\Debug;

//Debug::dd(Auth::is_logged());
if(Auth::is_logged()){
	if(array_key_exists('id',$_GET) && !empty($_GET['id']) ){
		$db = new DB;
		$array=[
			'frm_tbl'=>'employment_histories',
			'where'=>['id'=>$_GET['id'] , 'users_id'=>$_SESSION['user_logged_as'] ],
		];
		$allData=$db->select($array);
		if(count($allData)===1){
			$allData=$allData[0];
		}else{
			die("ERROR!!!");
		}
	}else{
		die("ERROR!");
	}
}else{
	header("location: ./../../");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>PULSE | RESPONSIVE ACADEMIC PERSONAL VCARD TEMPLATE</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- CSS | STYLE -->

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/linecons.css" />
    <link rel="stylesheet" type="text/css" href="css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="css/colors/brown.css" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />

    <!-- CSS | Google Fonts -->

    <link href='http://fonts.googleapis.com/css?family=Montserrat:400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:200,400,300,500,600' rel='stylesheet' type='text/css'>

    <noscript>
        <style>
        @media screen and (max-width: 755px) {
            .hs-content-scroller {
                overflow: visible;
            }
        }
        </style>
    </noscript>
</head>

<body>
    <!-- Page preloader -->
    <div id="page-loader">
        <canvas id="demo-canvas"></canvas>
    </div>
    <!-- container -->
    <div id="hs-container" class="hs-container">

        <!-- Sidebar-->
        <div class="aside1">
            <a class="contact-button"><i class="fa fa-paper-plane"></i></a>
            <a class="download-button"><i class="fa fa-cloud-download"></i></a>
            <div class="aside-content"><span class="part1">PULSE</span><span class="part2">Personal Vcard</span>
            </div>
        </div>
        <aside class="hs-menu" id="hs-menu">
            <!-- <canvas id="demo-canvas"></canvas> -->

            <!-- Profil Image-->
            <div class="hs-headline">
                <a id="my-link" href="#my-panel"><i class="fa fa-bars"></i></a>
                <a href="#" class="download"><i class="fa fa-cloud-download"></i></a>
                <div class="img-wrap">
                    <img src="images/portrait.jpg" alt="" width="150" height="150" />
                </div>
                <div class="profile_info">
                    <h1>Johnny smith</h1>
                    <h4>WEB DESIGNER</h4>
                    <h6><span class="fa fa-location-arrow"></span>&nbsp;&nbsp;&nbsp;San Francisco , CA</h6>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="separator-aside"></div>
            <!-- End Profil Image-->

            <!-- menu -->
            <nav>
                <a href="index.php?section=1"><span class="menu_name">ABOUT</span><span class="fa fa-home"></span> </a>
                <a href="index.php?section=2"><span class="menu_name">RESUME</span><span class="fa fa-newspaper-o"></span> </a>
                <a href="index.php?section=3"><span class="menu_name">SERVICES</span><span class="fa fa-laptop"></span> </a>
                <a href="index.php?section=4"><span class="menu_name">PROCESS &amp; TEAM</span><span class="fa fa-users"></span> </a>
                <a href="index.php?section=5"><span class="menu_name">PRICING</span><span class="fa fa-money"></span> </a>
                <a href="index.php?section=6"><span class="menu_name">SKILLS</span><span class="fa fa-diamond"></span> </a>
                <a href="index.php?section=7"><span class="menu_name">WORKS</span><span class="fa fa-briefcase"></span> </a>
                <a href="index.php?section=8"><span class="menu_name">CONTACT</span><span class="fa fa-paper-plane"></span> </a>
            </nav>
            <!-- end menu-->
            <!-- social icons -->
            <div class="aside-footer">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-linkedin"></i></a>
                <a href="#"><i class="fa fa fa-dribbble"></i></a>
                <a href="#"><i class="fa fa fa-github"></i></a>
            </div>
            <!-- end social icons -->
        </aside>
        <!-- End sidebar -->

        <!-- Go To Top Button -->
        <a href="#hs-menu" class="hs-totop-link"><i class="fa fa-chevron-up"></i></a>
        <!-- End Go To Top Button -->

        <!-- hs-content-scroller -->
        <div class="hs-content-scroller">
            <!-- Header -->
            <div id="header_container">
                <div id="header">
                    <div><a class="home"><i class="fa fa-home"></i></a>
                    </div>
                    <div><a href="" class="previous-page arrow"><i class="fa fa-angle-left"></i></a>
                    </div>
                    <div><a href="" class="next-page arrow"><i class="fa fa-angle-right"></i></a>
                    </div>
                    <!-- News scroll -->
                    <div class="news-scroll">
                        <span><i class="fa fa-line-chart"></i>RECENT ACTIVITY : </span>
                        <ul id="marquee" class="marquee">
                            <li>
                                Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce tincidunt adipiscing,massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce tincidunt adipiscing,massa.</li>
                            <li>
                                Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce tincidunt adipiscing,massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce tincidunt adipiscing,massa.
                            </li>
                            <li>
                                Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce tincidunt adipiscing,massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce tincidunt adipiscing,massa.
                            </li>
                            <li>
                                Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce tincidunt adipiscing,massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce tincidunt adipiscing,massa.
                            </li>
                            <li>
                                Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce tincidunt adipiscing,massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce tincidunt adipiscing,massa.
                            </li>
                            <li>
                                Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce tincidunt adipiscing,massa. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Fusce tincidunt adipiscing,massa.
                            </li>
                        </ul>
                    </div>
                    <!-- End News scroll -->
                </div>
            </div>
            <!-- End Header -->

            <!-- hs-content-wrapper -->
				<div class="hs-content-wrapper">
					<!-- About section -->
					<article class="hs-content about-section" id="section1">
						<span class="sec-icon fa fa-briefcase"></span>
						<div class="hs-inner">
							<span class="before-title">Edit Profile</span>
							<h2>Work & Employment History</h2>
							<div class="aboutInfo-contanier">

<?php

Debug::p($allData);

?>


        <form action="./../Process/editworks.php" method="post">
			<input type="number" name="id" value="<?php echo $allData->id;?>" />
			<div class="form-group">
			  <label class="control-label" for="company_name">Name of Company</label>
			  <input type="text" name="company_name" value="<?php echo $allData->company_name;?>" class="form-control" id="company_name" placeholder="Google Inc."/>
			</div>
			
			<div class="form-group">
			  <label class="control-label" for="company_business">Type</label>
			  <input type="text" name="company_business" value="<?php echo $allData->company_business;?>" class="form-control" id="company_business" placeholder="Govt. / NGO / Multi National"/>
			</div>
			
			<div class="form-group">
			  <label class="control-label" for="company_location">Location</label>
			  <input type="text" name="company_location" value="<?php echo $allData->company_location;?>" class="form-control" id="company_location" placeholder="Dhaka-1100"/>
			</div>
			
			<div class="form-group">
			  <label class="control-label" for="company_department">Field</label>
			  <input type="text" name="department" value="<?php echo $allData->department;?>" class="form-control" id="company_department" placeholder="IT"/>
			</div>
			
			<div class="form-group">
			  <label class="control-label" for="position">Position</label>
			  <input type="text" name="position" value="<?php echo $allData->position;?>" class="form-control" id="username" placeholder="Lead"/>
			</div>
			
			<div class="form-group">
			  <label class="control-label" for="expertise">Expertise Area</label>
			  <input type="text" name="area_of_experience" value="<?php echo $allData->area_of_experience;?>" class="form-control" id="expertise" placeholder="Networking"/>
			</div>
			
			<div class="form-group">
			  <label class="control-label" for="responsibilities">Responsibilities</label>
			  <input type="text" name="responsibilites" value="<?php echo $allData->responsibilites;?>" class="form-control" id="responsibilities" placeholder="Server Administration"/>
			</div>
			
			<div class="form-horizontal">
				<div class="form-group">

					<div class="col-md-6">
						<label for="date_from" class="col-sm-2 control-label">From</label>
						<div class="col-md-10">
						<input type="date" name="from" value="<?php echo $allData->from;?>" class="form-control" id="date_from">
						</div>
					</div>
					
					<div class="col-md-6">
						<label for="date_to" class="col-sm-2 control-label">To</label>
						<div class="col-md-10">
						<input type="date" name="to" value="<?php echo $allData->to;?>" class="form-control" id="date_to">
						</div>
					</div>
					
					<div class="col-md-12">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="date_present" value="<?php $checked=$allData->date_present===on?"checked":null; echo $cheked;?>"/> Presently I'm working in this company
								</label>
							</div>
					</div>
					
				</div>
			</div>
		
			<button type="submit" class="btn btn-primary"><i class="fa fa-plus">&nbsp;</i> Add</button>
			<button type="reset" class="btn btn-default" ><i class="fa fa-refresh">&nbsp;</i> Reset</button>
			<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove">&nbsp;</i> Close</button>
			
        </form>
							</div>
						</div>
						<br>
					</article>
					
				</div>
                <!-- End hs-content-wrapper -->
            </div>
            <!-- End hs-content-scroller -->
        </div>
        <!-- End container -->
        <div id="my-panel">
        </div>

        <!-- PLUGIN SCRIPTS -->

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/default.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript" src="js/watch.js"></script>
        <script type="text/javascript" src="js/layout.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

        <!-- END PLUGIN SCRIPTS -->
</body>

</html>
