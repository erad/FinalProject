<?php 

function __autoload($className){
	//echo $className."<br/>";
	$backSpaces = './../../../';
	$fileName = $backSpaces . str_replace("\\","/",$className) . ".php";
	//echo $fileName."<br/>";
	require_once($fileName);
}

session_start();

use App\Engine\Core\DB;
use App\Engine\Core\Auth;
use App\Engine\Tool\Msg;
use App\Engine\Tool\Debug;

//Debug::dd(Auth::is_logged());
if(Auth::is_logged()){
	$db = new DB;
	$array1=[
		'frm_tbl'=>'users',
		'where'=>['id'=>$_SESSION['user_logged_as'] ],
	];
	$userData=$db->select($array1);
	$array2=[
		'frm_tbl'=>'employment_histories',
		'where'=>['users_id'=>$_SESSION['user_logged_as'] ],
	];
	$employmentHistories=$db->select($array2);
}else{
	header("location: ./../../");
}


?>


<?php
require_once("./../../Library/fpdf/fpdf.php");

$pdf = new FPDF();
$pdf->AddPage();

$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,20,'Dynamic Resume!',1,1,'C');

$pdf->Ln(10);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,10,'Personal Information',0,1,'L');

$pdf->SetFont('Arial','B',10);
$pdf->Cell(50,10,'Name',0,0,'L');
$pdf->Cell(10,10,':',0,0,'C');
$pdf->SetFont('Arial','',10);
$pdf->Cell(100,10,'Full Name from Database',0,1,'L');

$pdf->SetFont('Arial','B',10);
$pdf->Cell(50,10,'Email',0,0,'L');
$pdf->Cell(10,10,':',0,0,'C');
$pdf->SetFont('Arial','',10);
$pdf->Cell(100,10,$userData[0]->email,0,1,'L');

$pdf->SetFont('Arial','B',10);
$pdf->Cell(50,10,'Date of Birth',0,0,'L');
$pdf->Cell(10,10,':',0,0,'C');
$pdf->SetFont('Arial','',10);
$pdf->Cell(100,10,'16th December, 1971',0,1,'L');

$pdf->Ln(10);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,10,'Employment history',0,1,'L');

foreach($employmentHistories AS $employHist)
{
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(50,10,'Company Name',0,0,'L');
	$pdf->Cell(10,10,':',0,0,'C');
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(100,10,$employHist->company_name.', '.$employHist->company_location,0,1,'L');
	
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(50,10,'Position',0,0,'L');
	$pdf->Cell(10,10,':',0,0,'C');
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(50,10,$employHist->position,0,0,'L');
	$pdf->Cell(50,10,'('.$employHist->from.' to '.$employHist->to.')',0,1,'L');
	$pdf->Ln(5);
	
}

$pdf->Output();
?>