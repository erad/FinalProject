<div class="panel panel-default">
    <div class="panel-body">
        <table id="myTable" class="table table-fixedheader table-bordered table-striped">
            <thead>
                <tr>
                    <th width="40%">Role</th>
                    <th width="40%">Company</th>
                    <th width="20%">Actions</th>
                </tr>
            </thead>
            <tbody style="height:110px">
			<?php 
			for($i=0;$i<20;$i++){
			?>
                <tr>
                    <td width="40%">Designation</td>
                    <td width="40%">Company</td>
                    <td width="20%">
						<a title="Edit" class="" href="#"><i class="fa fa-pencil fa-lg"></i></a>&nbsp;
						<a title="Remove" class="" href="#"><i class="fa fa-trash fa-lg"></i></a>
					</td>
                </tr>
			<?php };?>
            </tbody>
        </table>
    </div>
</div>