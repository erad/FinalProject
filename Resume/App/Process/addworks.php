<?php

function __autoload($className){
	//echo $className."<br/>";
	$backSpaces = './../../';
	$fileName = $backSpaces . str_replace("\\","/",$className) . ".php";
	//echo $fileName."<br/>";
	require_once($fileName);
}

use App\Engine\Core\DB;
use App\Engine\Core\Auth;
use App\Engine\Tool\Msg;
use App\Engine\Tool\Debug;


session_start();


if($_SERVER['REQUEST_METHOD']==='POST')
{
	$data=$_POST;
	//Debug::d($data);
	if( is_array($data) && count($data)===9 || count($data)===10
		&& array_key_exists('company_name',$data) && !empty($data['company_name'])
		&& array_key_exists('company_business',$data) && !empty($data['company_business'])
		&& array_key_exists('company_location',$data) && !empty($data['company_location'])
		&& array_key_exists('department',$data) && !empty($data['department'])
		&& array_key_exists('position',$data) && !empty($data['position'])
		&& array_key_exists('area_of_experience',$data) && !empty($data['area_of_experience'])
		&& array_key_exists('responsibilites',$data) && !empty($data['responsibilites'])
		&& array_key_exists('from',$data) && !empty($data['from'])
		&& array_key_exists('to',$data)
		&& array_key_exists('date_present',$data)
	){
		Debug::p($data);		
		$data['users_id']=$_SESSION['user_logged_as'];
		
		if(isset($data['date_present'])){
			$data['to']='0000-00-00';
		}
		Debug::p($data);
		
		$db = new DB;
		$array=[
			'tbl'=>'employment_histories',
			'set_values'=>$data,
		];
		$result = $db->insert($array);
		Debug::d($result);
		if($result){
			$html =  '
					<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<i class="fa fa-warning fa-lg"></i>&nbsp;
						<strong>Success!</strong> Employement history Added.
					</div>
				';			
				Msg::setMsg($html);
			header("location: ./../views/?section=7");	
			//echo "SUCCESS";
		}else{
			$html =  '
					<div class="alert alert-warning alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<i class="fa fa-warning fa-lg"></i>&nbsp;
						<strong>Error!</strong> Failed to add Employement history.
					</div>
				';			
				Msg::setMsg($html);
			header("location: ./../views/?section=7");	
			//echo "ERROR";
		}
		
	}else{
		$html =  '
				<div class="alert alert-info alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<i class="fa fa-warning fa-lg"></i>&nbsp;
					<strong>Error!</strong> Please fill out the form correctly.
				</div>
			';			
			Msg::setMsg($html);
	//header("location: ./../views/?section=7");	
	die('<h1 style="color:orange">Error!!!</h1>');
}
		
}else{
	//header("location: ./../../");	
	die('<h1 style="color:red">Error! Not Permitted!!!</h1>');
}

?>