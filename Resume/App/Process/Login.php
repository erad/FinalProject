<?php

function __autoload($className){
	//echo $className."<br/>";
	$backSpaces = './../../';
	$fileName = $backSpaces . str_replace("\\","/",$className) . ".php";
	//echo $fileName."<br/>";
	require_once($fileName);
}

session_start();

use App\Engine\Core\DB;
use App\Engine\Tool\Msg;
use App\Engine\Tool\Debug;



if($_SERVER['REQUEST_METHOD']==='POST')
{
	$data=$_POST;
	if(count($data)===2 && array_key_exists('username',$data) && array_key_exists('password',$data) && !empty($data['username']) && !empty($data['password']) )
	{
		$username=$data['username'];
		$password=$data['password'];
		$query_array = [
			'frm_tbl'=>'users',
			//'fields' => ['username','password' , 'email'],
			'where'=> [ 'user_name'=>$username , 'password'=>$password ],
		];
	
		$db = new DB;
		$dataFound =$db->select($query_array);
		
		if(count($dataFound)===1)
		{
			//Debug::p($dataFound);
			$_SESSION['user_logged_as']=$dataFound[0]->id;
			header("location: ./../../");
			
		}else{
				$html = '
					<div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<i class="fa fa-warning fa-lg"></i>&nbsp;
						<strong>Login Failed!</strong> Error or Invalid Login, Please try again later.
					</div>
				';
				//$_SESSION["flashMsg"]=$html;
				Msg::setMsg($html);
				header("location: ./../../");
			}
	}else{
		$html =  '
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<i class="fa fa-warning fa-lg"></i>&nbsp;
					<strong>Error!</strong> Please input your credential.
				</div>
			';			
			Msg::setMsg($html);
			header("location: ./../../");
	}
	
	
}else{
	header("location: ./../../");	
	die('<h1 style="color:red">Error! Not Permitted!!!</h1>');
}

?>