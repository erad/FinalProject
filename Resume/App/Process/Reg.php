<?php

function __autoload($className){
	//echo $className."<br/>";
	$backSpaces = './../../';
	$fileName = $backSpaces . str_replace("\\","/",$className) . ".php";
	//echo $fileName."<br/>";
	require_once($fileName);
}

use App\Engine\Core\DB;
use App\Engine\Core\Auth;
use App\Engine\Tool\Msg;
use App\Engine\Tool\Debug;


session_start();


if($_SERVER['REQUEST_METHOD']==='POST')
{
	$reg=$_POST;
	if(
		is_array($reg) &&
		count($reg)===4 &&
		array_key_exists('username',$reg) && !empty($reg['username']) &&
		array_key_exists('email',$reg) && !empty($reg['email']) &&
		array_key_exists('password',$reg) && !empty($reg['password']) &&
		array_key_exists('confirm_password',$reg) && !empty($reg['confirm_password'])
	)
	{
		
		
		if($reg['password']===$reg['confirm_password'])
		{
			$is_user_exists=Auth::is_exists(['frm_tbl'=>'users' , 'fields'=>['user_name'] , 'where'=>['user_name'=>$reg['username'] ] ] );
			$is_email_exists=Auth::is_exists(['frm_tbl'=>'users' , 'fields'=>['email'] , 'where'=>['email'=>$reg['email'] ] ] );
			
			//var_dump($is_user_exists);
			//var_dump($is_email_exists);
			$ex=true;
			if($is_user_exists){
				$msg="Username already exists... ";
				$ex=false;
			}else{
				$msg="";
			}
			if($is_email_exists){
				$msg .="Email already exists... ";
				$ex=false;
			}else{
				$msg .="";
			}
			//echo $ex;
			if($ex===false){
				$html =  '
					<div class="alert alert-danger alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<i class="fa fa-warning fa-lg"></i>&nbsp;
						<strong>Conflicted!</strong> '.$msg.'
					</div>
				';			
				Msg::setMsg($html);	
				header("location: ./../../");
			}else{
				//Debug::p($reg);
				$array=[
					'tbl'=>'users',
					'set_values'=>[
						'user_name'=>$reg['username'],
						'email'=>$reg['email'],
						'password'=>$reg['password'],
						'is_admin'=>0,
					],
				];
				$db = new DB;
				$result=$db->insert($array);
				if($result){
					$msg='
						<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<i class="fa fa-smile-o fa-lg"></i>&nbsp;
							<strong>Congratulation!</strong> You have been registered successfully. Now you can sign in with your USERNAME & PASSWORD.
						</div>
					';
					Msg::setMsg($msg);
					header("location: ../../");
				}else{
					$msg='
						<div class="alert alert-warning alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<i class="fa fa-smile-o fa-lg"></i>&nbsp;
							<strong>Sorry!</strong> Registration Failurem, please try again later.
						</div>
					';
					Msg::setMsg($msg);
					header("location: ../../");
				}
			}
			
		}else{
			$html =  '
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<i class="fa fa-warning fa-lg"></i>&nbsp;
					<strong>Password Missmatched!</strong> Please confirm correct password.
				</div>
			';			
			Msg::setMsg($html);
			header("location: ./../../");
		}
		
		
	}else{
			$html =  '
				<div class="alert alert-info alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<i class="fa fa-warning fa-lg"></i>&nbsp;
					<strong>Error!</strong> Fields Cannot Be Empty. Please fill out the form fields...
				</div>
			';			
			Msg::setMsg($html);
			header("location: ./../../");
		}
	
	
}else{
	header("location: ./../../");	
	die('<h1 style="color:red">Error! Not Permitted!!!</h1>');
}

?>