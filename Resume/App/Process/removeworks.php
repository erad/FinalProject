<?php 

function __autoload($className){
	//echo $className."<br/>";
	$backSpaces = './../../';
	$fileName = $backSpaces . str_replace("\\","/",$className) . ".php";
	//echo $fileName."<br/>";
	require_once($fileName);
}

session_start();

use App\Engine\Core\DB;
use App\Engine\Core\Auth;
use App\Engine\Tool\Msg;
use App\Engine\Tool\Debug;

//Debug::dd(Auth::is_logged());
if(Auth::is_logged()){
	if(array_key_exists('id',$_GET) && !empty($_GET['id']) ){
		$db = new DB;
		$array=[
			'tbl'=>'employment_histories',
			'where'=>['id'=>$_GET['id'] , 'users_id'=>$_SESSION['user_logged_as'] ],
		];
		$result=$allData=$db->remove($array);
		if($result){
			$html =  '
					<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<i class="fa fa-warning fa-lg"></i>&nbsp;
						<strong>Deleted!</strong> Record successfully deleted.
					</div>
				';			
				Msg::setMsg($html);
			header("location: ./../views/?section=7");	
		}else{
			$html =  '
					<div class="alert alert-warning alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<i class="fa fa-warning fa-lg"></i>&nbsp;
						<strong>Error!</strong> Failed to delete record.
					</div>
				';			
				Msg::setMsg($html);
			header("location: ./../views/?section=7");	
		}
	}else{
		die("ERROR!");
	}
}else{
	header("location: ./../../");
}

?>