<?php 

namespace App\Engine\Tool;

class Debug
{
	static public function d($param){
		echo "<pre>";
		var_dump($param);
		echo "</pre>";
	}
	
	static public function dd($param){
		self::d($param);
		die("Debugged && Died!");
	}
	
	static public function p($param){
		echo "<pre>";
		print_r($param);
		echo "</pre>";
	}
	
	static public function pd($param){
		self::p($param);
		die("Printed && Died!");
	}
	
}

?>