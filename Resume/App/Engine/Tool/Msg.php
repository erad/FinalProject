<?php 
namespace App\Engine\Tool;

class Msg
{
	static public function setMsg($param){
		$_SESSION['flashMsg']=$param;
	}
	
	static public function getMsg(){
		$msg=$_SESSION['flashMsg'];
		unset($_SESSION['flashMsg']);
		return $msg;
	}
	
}

?>