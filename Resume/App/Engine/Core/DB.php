<?php

namespace App\Engine\Core;


function __autoload($className){
	//echo $className."<br/>";
	$backSpaces = './../../';
	$fileName = $backSpaces . str_replace("\\","/",$className) . ".php";
	//echo $fileName."<br/>";
	require_once($fileName);
}


use App\Engine\Tool\Msg;

class DB
{
	public function __construct()
	{
		mysql_connect("127.0.0.1" , "root" , "") or die('<h1 style="color:red">Failed To Connect Database Server!!!</h1>');
		mysql_select_db("resume")  or die('<h1 style="color:red">Database Not Found!!!</h1>');
		//echo "Database Connected!";
	}
	
	public function select($array){
		if(is_array($array) && array_key_exists('frm_tbl',$array) && is_string($array['frm_tbl']) && !empty($array['frm_tbl']) )
		{
			$tbl = $array['frm_tbl'];
			if(array_key_exists('fields',$array) && is_array($array['fields']) && count($array['fields'])>0  )
			{
				foreach($array['fields'] AS $field){
					$fields[]="`".$field."`";
				}
				$fields=implode(' , ' , $fields);
				$query = " SELECT " . $fields . " FROM `".$tbl."`";
			}else{
				$query = " SELECT * FROM `".$tbl."`";
			}
			if(array_key_exists('where',$array) && is_array($array['where']) && count($array['where'])>0  )
			{
				foreach($array['where'] AS $k=>$v){
					$wheres[]="`".$k."`='".$v."'";
				}
				$wheres=implode(' AND ' , $wheres);
				$query .= " WHERE " . $wheres;
			}
			
			$finalQuery = $query ;
			//echo $finalQuery; die("Query Printed & Execution Died!");
			
			$result = mysql_query($finalQuery);
			if($result){
				$allRows = [];
				while($row=mysql_fetch_object($result)){
					$allRows[]=$row;
				}
				return $allRows;
			}else{
				die('<h1 style="color:red">Database Error!!!</h1>');
			}
			
		}else{
			echo "ERROR! Table name must be defined!";
		}
	}
	
	public function insert($array){
		//var_dump($array);
		if(is_array($array) && count($array)===2 )
		{
			if( array_key_exists('tbl',$array) && !empty($array['tbl']) )
			{
				$tbl = $array['tbl'];
				if( array_key_exists('set_values',$array) && is_array($array['set_values']) && count($array['set_values'])>0 )
				{
					foreach($array['set_values'] AS $k=>$v){
						$keys[]="`".$k."`";
						$values[]="'".$v."'";
					}
					
					$query = "INSERT INTO `".$tbl."` (".implode(' , ',$keys).") VALUES (".implode(' , ',$values).")";
					$result=mysql_query($query);
					//echo $query;die();
					if($result){
						return true;
					}else{
						return false;
					}
					
				}else{
					die('Values against Keys must be defined');
				}
			}else{
				die('Table name must be defined');
			}
		}
	}
	
	public function remove($array)
	{
		if(array_key_exists('tbl',$array) && !empty($array['tbl']) ){
			$tbl=$array['tbl'];
			if(array_key_exists('where',$array) && !empty($array['where']) && is_array($array['where']) && count($array['where'])>0 )
			{
				foreach($array['where'] AS $k=>$v){
					$wheres[]="`".$k."`='".$v."'";
				}
				$whq = implode(' AND ',$wheres);
			}
		}
		$query = "DELETE FROM `".$tbl."` WHERE ".$whq;
		$result=mysql_query($query);
		if($result){
			return true;
		}else{
			return false;
		}
	}
	
	public function update($array)
	{
		if(is_array($array) && array_key_exists('tbl',$array) )
		{
			$tbl = $array['tbl'];
			if(array_key_exists('update',$array)  && count($array['update'])>0 )
			{
				foreach($array['update'] AS $k=>$v){
					$keyvalues[]="`".$k."`='".$v."'";					
				}
				$updates = implode(' , ',$keyvalues);
				$query = " UPDATE `".$tbl."` SET ".$updates.' ';
			}
			
			if(array_key_exists('where',$array) && is_array($array['where']) && count($array['where'])>0  )
			{
				foreach($array['where'] AS $k=>$v){
					$wheres[]="`".$k."`='".$v."'";
				}
				$wheres=implode(' AND ' , $wheres);
				$query .= " WHERE " . $wheres;
			}
			$result=mysql_query($query);
			if($result){
				return true;
			}else{
				return false;
			}
		}
	}
	
}

?>