<?php 

namespace App\Engine\Core;

use App\Engine\Core\DB;

class Auth
{
	static public function is_logged()
	{
		if(isset($_SESSION['user_logged_as']) && !is_null($_SESSION['user_logged_as']) && !empty($_SESSION['user_logged_as']) ) {
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	static public function is_exists($array)
	{
		if(is_array($array)){
			$db=new DB;
			$result=$db->select($array);
			if(count($result)>0){
				return TRUE;
			}else{
				return FALSE;
			}
		}
	}
}

?>