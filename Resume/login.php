<?php 

function __autoload($className){
	//echo $className."<br/>";
	$backSpaces = '';
	$fileName = $backSpaces . str_replace("\\","/",$className) . ".php";
	//echo $fileName."<br/>";
	require_once($fileName);
}

session_start();

use App\Engine\Core\DB;
use App\Engine\Core\Auth;
use App\Engine\Tool\Msg;
use App\Engine\Tool\Debug;

if(Auth::is_logged()){
	header("location: App/Views/");
}


?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Login</title>

		<!-- Bootstrap -->
		<link href="App/Resources/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="App/Resources/font-awesome/css/font-awesome.min.css" rel="stylesheet">

		<style type="text/css">
			body{
					background:RGB(240,240,240);
					color:;
			}

		</style>

	</head>
        
	<body class="">
				
				
		<nav class="navbar navbar-default">
		  <div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="#">
						<i class="fa fa-user"></i>&nbsp;My Portfolio
			  </a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  
			  <form class="navbar-form navbar-right" role="search" action="App/Process/Login.php" method="POST">
				<div class="form-group">
				  <input type="text" name="username" class="form-control input-sm" placeholder="username" autofocus/>
				</div>
				<div class="form-group">
				  <input type="password" name="password" class="form-control input-sm" placeholder="password"/>
				</div>
				<button type="submit" class="btn btn-default btn-sm hidden-lg hidden-md"><i class="fa fa-lg fa-sign-in"></i></button>
			  </form>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	
		<div class="container">
		
			<div class="row ">
				<?php
					$msg=isset($_SESSION['flashMsg'])?Msg::getMsg():NULL;
					echo $msg;
				?>
			</div>
			
		
				<div class="col-md-8">
											
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					  <!-- Indicators -->
					  <ol class="carousel-indicators">
						<li data-target="#carousel-example-generic" data-slide-to="0"></li>
						<li data-target="#carousel-example-generic" data-slide-to="1" class="active"></li>
						<li data-target="#carousel-example-generic" data-slide-to="2"></li>
					  </ol>

					  <!-- Wrapper for slides -->
					  <div class="carousel-inner" role="listbox">
						<div class="item">
						  <img src="App/Resources/images/slide/slide0.jpg" alt="..." width="100%"/>
						  <div class="carousel-caption">
							...
						  </div>
						</div>
						<div class="item active">
						  <img src="App/Resources/images/slide/slide1.jpg" alt="..." width="100%"/>
						  <div class="carousel-caption">
							...
						  </div>
						</div>
						<div class="item">
						  <img src="App/Resources/images/slide/slide2.jpg" alt="..." width="100%"/>
						  <div class="carousel-caption">
							...
						  </div>
						</div>
					  </div>

					  <!-- Controls -->
					  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					  </a>
					  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					  </a>
					</div>
				
				</div>
				<div class="col-md-4">
						<form action="App/Process/Reg.php" method="POST">
								<fieldset>
										<legend>
											<i class="fa fa-question-circle"></i>&nbsp;
											Need an account? Sign Up Here...
										</legend>
										
										<div class="form-group">
										  <label class="control-label" for="username">Username</label>
										  <input type="text" name="username" class="form-control" id="username"/>
										</div>
										
										<div class="form-group">
										  <label class="control-label" for="email">Email</label>
										  <input type="email" name="email" class="form-control" id="email"/>
										</div>
										
										<div class="form-group">
										  <label class="control-label" for="password">Password</label>
										  <input type="password" name="password" class="form-control" id="password"/>
										</div>
										
										<div class="form-group">
										  <label class="control-label" for="confirm_password_area">Confirm Password</label>
										  <input type="password" name="confirm_password" class="form-control" id="confirm_password"/>
										</div>
										
										<div class="form-group">
												<button type="submit" class="btn btn-success"><i class="fa fa-spinner"></i>&nbsp;Submit</button>
												<button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i>&nbsp;Reset</button>
										</div>
								</fieldset>
						</form>
				</div>
		</div>
		
		<div class="bg-danger">
			<footer class="bg-foot">
				<div class="container">
					<h5>&copy; The Code Squad</h5>
				</div>
			</footer>
		</div>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="App/Resources/js/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="App/Resources/bootstrap/js/bootstrap.min.js"></script>
	</body>
        
</html>
