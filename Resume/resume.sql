-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2016 at 02:32 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `resume`
--

-- --------------------------------------------------------

--
-- Table structure for table `academic_qualifications`
--

CREATE TABLE IF NOT EXISTS `academic_qualifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `level_of_education` varchar(255) NOT NULL,
  `degree_title` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `institute_name` varchar(255) NOT NULL,
  `result` varchar(255) NOT NULL,
  `year_of_passing` date NOT NULL,
  `duration` varchar(255) NOT NULL,
  `achievement` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `career_n_app_informations`
--

CREATE TABLE IF NOT EXISTS `career_n_app_informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `objective` varchar(255) NOT NULL,
  `present_sal` int(11) NOT NULL,
  `expected_sal` int(11) NOT NULL,
  `looking_for` varchar(255) NOT NULL,
  `available_for` varchar(255) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employment_histories`
--

CREATE TABLE IF NOT EXISTS `employment_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_business` varchar(255) NOT NULL,
  `company_location` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `area_of_experience` varchar(255) NOT NULL,
  `responsibilites` varchar(255) NOT NULL,
  `from` varchar(10) NOT NULL,
  `to` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `employment_histories`
--

INSERT INTO `employment_histories` (`id`, `users_id`, `company_name`, `company_business`, `company_location`, `department`, `position`, `area_of_experience`, `responsibilites`, `from`, `to`) VALUES
(11, 5, 'Toa Toa Company Unlimited', 'Multi National', 'Anywhere', 'Any Field', 'Manager', 'Wandering', ';)', '2016-01-31', '2016-01-31'),
(12, 7, 'Pran Food Company', 'Multi National', 'Dhaka', 'Foods & Beverage', 'Salesman', 'Marketing', 'Marketing', '2012-12-24', '2014-03-05');

-- --------------------------------------------------------

--
-- Table structure for table `others`
--

CREATE TABLE IF NOT EXISTS `others` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `languages` varchar(255) NOT NULL,
  `reference` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `other_relevent_informations`
--

CREATE TABLE IF NOT EXISTS `other_relevent_informations` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `career_summary` varchar(255) NOT NULL,
  `special_qualification` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `personal_details`
--

CREATE TABLE IF NOT EXISTS `personal_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `mother_name` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` varchar(255) NOT NULL,
  `marital_status` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `national_id_no` int(11) NOT NULL,
  `religion` varchar(255) NOT NULL,
  `present_address` varchar(255) NOT NULL,
  `permanent_address` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `home_phone` int(11) NOT NULL,
  `mobile` int(11) NOT NULL,
  `office_phone` int(11) NOT NULL,
  `alternative_email` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `modified_at` date NOT NULL,
  `deleted_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photographs`
--

CREATE TABLE IF NOT EXISTS `photographs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `photo_name` varchar(255) NOT NULL,
  `photo_url` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `preferred_areas`
--

CREATE TABLE IF NOT EXISTS `preferred_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NOT NULL,
  `inside_bangladesh` varchar(255) NOT NULL,
  `outside_bangladesh` varchar(255) NOT NULL,
  `organization_type` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_admin` tinyint(4) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `is_admin`, `user_name`, `password`, `email`) VALUES
(1, 0, 'user1', 'password', 'user1@email.com'),
(2, 1, 'user2', 'password', 'user2@email.com'),
(5, 0, 'amin', 'hello', 'amin@email.com'),
(6, 0, 'wahed', 'sarker', 'wahed@email.com'),
(7, 0, 'mamun', '1234', 'mamun@email.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
